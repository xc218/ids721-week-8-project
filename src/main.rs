use csv::ReaderBuilder;
use serde::Deserialize;
use std::error::Error;

// Define a structure that represents a row in your CSV.
#[derive(Debug, Deserialize)]
struct Record {
    value: f64,
}

fn main() -> Result<(), Box<dyn Error>> {
    let file_path = "data.csv";
    let mut rdr = ReaderBuilder::new().from_path(file_path)?;
    
    let mut sum = 0.0;
    for result in rdr.deserialize() {
        let record: Record = result?;
        sum += record.value;
    }

    println!("Sum: {}", sum);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sum() {
        // This is a placeholder for your actual tests
        assert_eq!(2 + 2, 4);
    }
}

