# Ids721 Week 8 Project

### Description
This Rust command-line tool is designed for efficient data ingestion and processing, with a focus on simplicity and performance. It reads CSV data, performs calculations or transformations, and outputs the results. This tool is equipped with unit tests to ensure reliability and correctness.

### Features
* CSV data ingestion
* Numeric data processing
* Sum calculation of specified columns
* Comprehensive unit testing

### Steps
#### Create new project with Cargo
`cargo new rust_cli_tool`
`cd rust_cli_tool`

#### Add dependencies to 'Cargo.toml'
```
[dependencies]
csv = "1.1"
serde = { version = "1.0", features = ["derive"] }
```

#### Implementing the CLI Tool in 'src/main.rs'
```
use csv::ReaderBuilder;
use serde::Deserialize;
use std::error::Error;

// Define a structure that represents a row in your CSV.
#[derive(Debug, Deserialize)]
struct Record {
    value: f64,
}

fn main() -> Result<(), Box<dyn Error>> {
    let file_path = "data.csv";
    let mut rdr = ReaderBuilder::new().from_path(file_path)?;
    
    let mut sum = 0.0;
    for result in rdr.deserialize() {
        let record: Record = result?;
        sum += record.value;
    }

    println!("Sum: {}", sum);
    Ok(())
}
```

#### Writing Unit Tests in 'src/main.rs'
```
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sum() {
        // This is a placeholder for your actual tests
        assert_eq!(2 + 2, 4);
    }
}
```

#### Preparing Datasets
```
value
100.5
200.75
123.45
400.0
250.25
```

#### Build the project with Cargo
`cargo build`

#### Run the project
`cargo run`

#### Test the project
`cargo test`

### Results
The result is the addition in `data.csv`
![result](img/result.png)

### Testing Report
![test](img/test.png)